#!/bin/bash

read -p "1 - Deseja criar a estrutura do banco de dados MySQL? (Tecle S para Sim) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Ss]$ ]]
then
    mysql -u root -p buscasus < busca_sus.sql
fi

read -p "2 - Deseja rodar o crawler para pegar as instituicoes do CNES no portal Datasus e salvar no 1-instituicoes.json? (Tecle S para Sim) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Ss]$ ]]
then
    scrapy crawl cnes -o json/1-instituicoes-piaui.json -t json
fi

read -p "3 - Deseja inserir as instituicoes do arquivo 1-instituicoes.json no banco de dados MySQL? (Tecle S para Sim) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Ss]$ ]]
then
    python insert_instituicoes.py
fi

read -p "4 - Deseja atualizar no banco de dados a tabela cidade com as cidades das instituicoes cadastradas? (Tecle S para Sim) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Ss]$ ]]
then
    python update_cidade.py
fi

read -p "5 - Deseja rodar o crawler para pegar os tipos de atendimento das instituicoes e salvar no 2-atendimentos.json? (Tecle S para Sim) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Ss]$ ]]
then
    scrapy crawl cnesatendimento -o json/2-atendimentos.json -t json
fi

read -p "6 - Deseja rodar o crawler para pegar os dados de geolocalizacao das instituicoes e salvar no 3-coordenadas.json? (Tecle S para Sim) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Ss]$ ]]
then
    scrapy crawl cnesmap -o json/3-coordenadas.json -t json
fi

read -p "7 - Deseja atualizar o banco de dados com o conteudo do arquivo 2-atendimentos.json obtido marcando os atendimentos realizados pelo SUS de cada instituicao? (Tecle S para Sim) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Ss]$ ]]
then
    python update_atendimento.py
fi

read -p "8 - Deseja atualizar no banco de dados as instituicoes com os dados de geolocalizacao que estao no arquivo 3-coordenadas.json? (Tecle S para Sim) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Ss]$ ]]
then
    python update_coords.py
fi

read -p "9 - Deseja atualizar as instituicoes determinando o tipo de estabelecimento? (Tecle S para Sim) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Ss]$ ]]
then
    python update_tipo_estabelecimento.py
fi