import MySQLdb as mdb

class updateTipoEstabelecimento:
	
	def atualizar(self):

		con = mdb.connect('127.0.0.1', 'root', '', 'buscasus');
		cur = con.cursor()
		
		cur.execute("SELECT _id, tipo_estabelecimento FROM instituicao ORDER BY _id ASC")
		data = cur.fetchall()
		for item in data:
			# print item[1]
			cur.execute("SELECT _id,descricao FROM tipo_estabelecimento WHERE descricao=%s",[item[1]])
			data2 = cur.fetchall()
			if(len(data2) <= 0):
				cur.execute("INSERT INTO tipo_estabelecimento (descricao) VALUES (%s)",[item[1]])
				con.commit()

				cur.execute("SELECT _id FROM tipo_estabelecimento ORDER BY _id DESC LIMIT 1")
				data3 = cur.fetchall()

				cur.execute("UPDATE instituicao SET id_tipo_estabelecimento=%r WHERE _id=%r",[data3[0][0],item[0]])
				con.commit()
			else:
				cur.execute("UPDATE instituicao SET id_tipo_estabelecimento=%r WHERE _id=%r",[data2[0][0],item[0]])
				con.commit()
		con.close()

instancia = updateTipoEstabelecimento()
instancia.atualizar()