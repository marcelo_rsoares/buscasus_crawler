import requests, json
import MySQLdb as mdb
import sys

class updateAtendimento:
	
	def atualizar(self):
		r=open('json/2-atendimentos.json')
		j = json.load(r)

		con = mdb.connect('127.0.0.1', 'root', '', 'buscasus');
		cur = con.cursor()
		
		for feed in j:
			# print feed['VUnidade']
			cur.execute("SELECT cnes, nome, VUnidade FROM instituicao WHERE VUnidade="+feed['VUnidade'])
			data = cur.fetchall()
			if(len(data) > 0):
				if 'ambulatorial' in feed:
						print 'ambulatorial: '
						print feed['ambulatorial']
						if(feed['ambulatorial'] == "SUS"):
							ambulatorial = 1
						else:
							ambulatorial = 0
						cur.execute("UPDATE instituicao SET ambulatorial=%s WHERE VUnidade=%s",(ambulatorial,feed['VUnidade']))

				if 'internacao' in feed:
						print 'internacao: '
						print feed['internacao']
						if(feed['internacao'] == "SUS"):
							internacao = 1
						else:
							internacao = 0
						cur.execute("UPDATE instituicao SET internacao=%s WHERE VUnidade=%s",(internacao,feed['VUnidade']))

				if 'urgencia' in feed:
						print 'urgencia: '
						print feed['urgencia']
						if(feed['urgencia'] == "SUS"):
							urgencia = 1
						else:
							urgencia = 0
						cur.execute("UPDATE instituicao SET urgencia=%s WHERE VUnidade=%s",(urgencia,feed['VUnidade']))

				if 'sadt' in feed:
						print 'sadt: '
						print feed['sadt']
						if(feed['sadt'] == "SUS"):
							sadt = 1
						else:
							sadt = 0
						cur.execute("UPDATE instituicao SET sadt=%s WHERE VUnidade=%s",(sadt,feed['VUnidade']))

				con.commit()


		cur.execute("DELETE FROM instituicao WHERE (urgencia < 1) AND (ambulatorial < 1) AND (internacao < 1) AND (urgencia < 1) AND (sadt < 1)")

		con.close()

instancia = updateAtendimento()
instancia.atualizar()