import requests, json
import MySQLdb as mdb
import sys
from unicodedata import normalize

class UpdateCidade:
	
	def atualizar(self):

		con = mdb.connect('127.0.0.1', 'root', '', 'buscasus');
		cur = con.cursor()
		
		cur.execute("SELECT _id,municipio,uf,latitude,longitude FROM instituicao ORDER BY _id ASC")
		data = cur.fetchall()

		for instituicao in data:
			cidade = instituicao[1].split(" - ")
			# print type(cidade[0])
			# print "SELECT _id FROM cidade WHERE nome='%s' LIMIT 1" % (str(cidade[0]))
			cur.execute("SELECT _id FROM cidade WHERE nome=%s LIMIT 1", [str(cidade[0])])
			municipio = cur.fetchall()
			con.commit()

			if(len(municipio) > 0):
				# print "UPDATE instituicao SET municipio='%s' WHERE _id=%s" % (municipio[0][0],instituicao[0])
				cur.execute("UPDATE instituicao SET municipio='%s' WHERE _id=%s", (municipio[0][0],instituicao[0]))
			else:
				# print "INSERT INTO cidade (nome,uf,_id) VALUES (%s,%s,%s)" % (cidade[0],instituicao[2],cidade[2])
				cur.execute("INSERT INTO cidade (nome,uf,_id) VALUES (%s,%s,%s)", [cidade[0],instituicao[2],cidade[2]])

				# print "UPDATE instituicao SET municipio=%s WHERE _id=%s" % (cidade[2],instituicao[0])
				cur.execute("UPDATE instituicao SET municipio=%s WHERE _id=%s", [cidade[2],instituicao[0]])

				con.commit()

		cur.execute("SELECT _id,nome,uf FROM cidade ORDER BY _id ASC")
		data2 = cur.fetchall()
		
		for item in data2:
			print item
			print "http://maps.googleapis.com/maps/api/geocode/json?address=%s,%s&sensor=false" % (item[1],item[2])
		
			response = requests.get("http://maps.googleapis.com/maps/api/geocode/json?address=%s,%s&sensor=false" % (item[1],item[2]))
			coords = response.json()
			for results in coords.iteritems():

				if(type(results[1]) == type(list())):
					result_result = results[1]

				else:
					result_status = results[1]

			if (result_status == 'OK'):
				cidade1 =  result_result[0]['address_components'][0]['long_name'].encode('utf-8')
				cidade = normalize('NFKD', cidade1.decode('utf-8')).encode('ASCII','ignore')
				if(cidade.upper() == item[1]):
					latitude = result_result[0]['geometry']['location']['lat']
					longitude =  result_result[0]['geometry']['location']['lng']
					print "UPDATE cidade SET latitude=%f, longitude=%f WHERE _id=%d" % (latitude,longitude,item[0])
					cur.execute("UPDATE cidade SET latitude=%s, longitude=%s WHERE _id=%s",[latitude,longitude,item[0]])
					con.commit()

		con.close()

instancia = UpdateCidade()
instancia.atualizar()