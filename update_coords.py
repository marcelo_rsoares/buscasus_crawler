import requests, json
import MySQLdb as mdb
import sys

class updateCoords:
	
	def atualizar(self):

		r=open('json/3-coordenadas.json')
		j = json.load(r)

		con = mdb.connect('127.0.0.1', 'root', '', 'buscasus');
		cur = con.cursor()
		
		for feed in j:
			cur.execute("SELECT cnes, nome,VUnidade FROM instituicao WHERE VUnidade=%s LIMIT 1",[feed['VUnidade']])
			data = cur.fetchall()
			if(len(data) > 0):
				if(len(feed['latitude']) > 0):
					print "UPDATE instituicao SET latitude=%s WHERE VUnidade=%s" % (feed['latitude'],feed['VUnidade'])
					cur.execute("UPDATE instituicao SET latitude=%s WHERE VUnidade=%s",[feed['latitude'],feed['VUnidade']])

				if(len(feed['longitude']) > 0):
					print "UPDATE instituicao SET longitude=%s WHERE VUnidade=%s" % (feed['longitude'],feed['VUnidade'])
					cur.execute("UPDATE instituicao SET longitude=%s WHERE VUnidade=%s",[feed['longitude'],feed['VUnidade']])
				con.commit()
		con.close()

instancia = updateCoords()
instancia.atualizar()