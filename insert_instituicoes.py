import requests, json, datetime
import MySQLdb as mdb
import sys

class importToMysql:
	
	def importar(self):
		r=open('json/1-instituicoes-piaui.json')
		j = json.load(r)

		con = mdb.connect('127.0.0.1', 'root', '', 'buscasus');
		cur = con.cursor()
		
		for feed in j:
			print feed
			cur.execute("SELECT cnes FROM instituicao WHERE cnes="+feed['cnes'][0])
			data = cur.fetchall()
			if(len(data) <= 0):

				cur.execute("INSERT INTO instituicao (cnes) VALUES ("+feed['cnes'][0]+")")

				if(len(feed['uf']) > 0):
					cur.execute("UPDATE instituicao SET uf=%s WHERE cnes=%s",(feed['uf'][0],feed['cnes'][0]))

				if(len(feed['municipio']) > 0):
					cur.execute("UPDATE instituicao SET municipio=%s WHERE cnes=%s",(feed['municipio'][0],feed['cnes'][0]))

				if(len(feed['nome']) > 0):
					cur.execute("UPDATE instituicao SET nome=%s WHERE cnes=%s",(feed['nome'][0],feed['cnes'][0]))

				if(len(feed['bairro']) > 0):
					cur.execute("UPDATE instituicao SET bairro=%s WHERE cnes=%s",(feed['bairro'][0],feed['cnes'][0]))

				if(len(feed['logradouro']) > 0):
					cur.execute("UPDATE instituicao SET logradouro=%s WHERE cnes=%s",(feed['logradouro'][0],feed['cnes'][0]))

				if(len(feed['nome_empresarial']) > 0):
					cur.execute("UPDATE instituicao SET nome_empresarial=%s WHERE cnes=%s",(feed['nome_empresarial'][0],feed['cnes'][0]))
				
				if(len(feed['cep']) > 0):
					cur.execute("UPDATE instituicao SET cep=%s WHERE cnes=%s",(feed['cep'][0],feed['cnes'][0]))

				if(len(feed['numero']) > 0):
					cur.execute("UPDATE instituicao SET numero=%s WHERE cnes=%s",(feed['numero'][0],feed['cnes'][0]))

				if(len(feed['complemento']) > 0):
					cur.execute("UPDATE instituicao SET complemento=%s WHERE cnes=%s",(feed['complemento'][0],feed['cnes'][0]))

				if(len(feed['cnpj']) > 0):
					cur.execute("UPDATE instituicao SET cnpj=%s WHERE cnes=%s",(feed['cnpj'][0],feed['cnes'][0]))

				if(len(feed['natureza_da_organizacao']) > 0):
					cur.execute("UPDATE instituicao SET natureza_da_organizacao=%s WHERE cnes=%s",(feed['natureza_da_organizacao'][0],feed['cnes'][0]))

				if(len(feed['telefone']) > 0):
					cur.execute("UPDATE instituicao SET telefone=%s WHERE cnes=%s",(feed['telefone'][0],feed['cnes'][0]))

				if(len(feed['tipo_estabelecimento']) > 0):
					cur.execute("UPDATE instituicao SET tipo_estabelecimento=%s WHERE cnes=%s",(feed['tipo_estabelecimento'][0],feed['cnes'][0]))

				if(len(feed['subtipo_estabelecimento']) > 0):
					cur.execute("UPDATE instituicao SET subtipo_estabelecimento=%s WHERE cnes=%s",(feed['subtipo_estabelecimento'][0],feed['cnes'][0]))

				if(len(feed['data_cadastrado_cnes']) > 0):
					data_cadastrado_cnes = datetime.datetime.strptime(feed['data_cadastrado_cnes'],"%d/%m/%Y").strftime('%Y-%m-%d')
					cur.execute(u"UPDATE instituicao SET data_cadastrado_cnes=%s WHERE cnes=%s",(data_cadastrado_cnes,feed['cnes'][0]))

				if(len(feed['data_atualizado_cnes']) > 0):
					data_atualizado_cnes = datetime.datetime.strptime(feed['data_atualizado_cnes'],"%d/%m/%Y").strftime('%Y-%m-%d')
					cur.execute(u"UPDATE instituicao SET data_atualizado_cnes=%s WHERE cnes=%s",(data_atualizado_cnes,feed['cnes'][0]))

				if(len(feed['id_estado']) > 0):
					cur.execute("UPDATE instituicao SET id_estado=%s WHERE cnes=%s",(feed['id_estado'],feed['cnes'][0]))

				if(len(feed['id_cidade']) > 0):
					cur.execute("UPDATE instituicao SET id_cidade=%s WHERE cnes=%s",(feed['id_cidade'],feed['cnes'][0]))

				if(len(feed['id_cidade']) > 0):	
					cur.execute("UPDATE instituicao SET VUnidade=%s WHERE cnes=%s",(feed['id_cidade']+feed['cnes'][0],feed['cnes'][0]))

				con.commit()

			if(len(feed['id_cidade']) > 0):	
				VUnidade = feed['id_cidade']+feed['cnes'][0]
				cur.execute("UPDATE instituicao SET VUnidade=%s WHERE cnes=%s",(VUnidade,feed['cnes'][0]))
				con.commit()

		con.close()

instancia = importToMysql()
instancia.importar()