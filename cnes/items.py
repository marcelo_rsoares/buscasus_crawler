# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field

class CnesItem(scrapy.Item):
	nome = Field()
	cnes = Field()
	cnpj = Field()
	nome_empresarial = Field()
	# cpf = Field()
	logradouro = Field()
	numero = Field()
	complemento = Field()
	bairro = Field()
	cep = Field()
	municipio = Field()
	# personalidade = Field()
	latitude = Field()
	longitude = Field()
	id_estado = Field()
	id_cidade = Field()
	telefone = Field()
	uf = Field()
	natureza_da_organizacao = Field()
	data_cadastrado_cnes = Field()
	data_atualizado_cnes = Field()
	tipo_estabelecimento = Field()
	subtipo_estabelecimento = Field()

class CnesItemMap(scrapy.Item):
	VUnidade = Field()
	latitude = Field()
	longitude = Field()

class CnesItem2(scrapy.Item):
	nome = Field()

class IdItem(scrapy.Item):
	id_estado = Field()
	id_cidade = Field()

class AtendimentoItem(scrapy.Item):
	ambulatorial = Field()
	internacao = Field()
	sadt = Field()
	urgencia = Field()
	demanda = Field()
	VUnidade = Field()