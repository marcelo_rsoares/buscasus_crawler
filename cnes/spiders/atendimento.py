from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy import Request
from urlparse import urlparse, parse_qs
from cnes.items import AtendimentoItem
import requests, json
import MySQLdb as mdb
import sys
from pprint import pprint

class CnesSpiderMap(Spider):
    name =  "cnesatendimento"
    allowed_domains = ["datasus.gov.br"]

    con = mdb.connect('127.0.0.1', 'root', '', 'buscasus');
    cur = con.cursor()

    start_urls = [
    ]

    cur.execute("SELECT VUnidade FROM instituicao ORDER BY VUnidade ASC")
    data = cur.fetchall()

    if(len(data) > 0):
        for elem in data:
            # print elem[0]            
            # "http://cnes.datasus.gov.br/cabecalho_reduzido.asp?VCod_Unidade="
            start_urls.append("http://cnes.datasus.gov.br/Mod_Bas_Atendimento.asp?VCo_Unidade="+elem[0])

    def parse(self, response):

        item = AtendimentoItem()
        query = parse_qs(urlparse(response.url).query)
        item['VUnidade'] = query['VCo_Unidade'][0]
        todos_os_itens = response.xpath("//td[@width='40%'][position()>1][position()<last()]/font/node()").extract()
        todos_os_itens2 = response.xpath("//td[@width='30%'][position()>1][position()<last()]/font/node()").extract()
        for i in xrange(len(todos_os_itens)):
            if(todos_os_itens[i] == 'AMBULATORIAL'):
                item['ambulatorial'] = todos_os_itens2[i]
            if(todos_os_itens[i] == 'INTERNACAO'):
                item['internacao'] = todos_os_itens2[i]
            if(todos_os_itens[i] == 'SADT'):
                item['sadt'] = todos_os_itens2[i]
            if(todos_os_itens[i] == 'URGENCIA'):
                item['urgencia'] = todos_os_itens2[i]

        item['demanda'] = response.xpath("//td[@width='40%'][last()]/font/text()").extract()
        # item['internacao'] = response.xpath('//table[9]/table[2]/table[4]/tr[3]/td[2]/font/text()').extract()
        # item['sadt'] = response.xpath('//table[9]/table[2]/table[4]/tr[4]/td[2]/font/text()').extract()
        # item['urgencia'] = response.xpath('//table[9]/table[2]/table[4]/tr[5]/td[2]/font/text()').extract()
        return AtendimentoItem(item)