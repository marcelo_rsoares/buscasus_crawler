from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy import Request
from urlparse import urlparse, parse_qs
from cnes.items import CnesItem, IdItem
import re
import HTMLParser
from unicodedata import normalize

class CnesSpider(Spider):
    name =  "cnes"
    allowed_domains = ["datasus.gov.br"]
    start_urls = [
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=12&NomeEstado=ACRE",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=27&NomeEstado=ALAGOAS",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=16&NomeEstado=AMAPA",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=13&NomeEstado=AMAZONAS",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=29&NomeEstado=BAHIA",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=23&NomeEstado=CEARA",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=53&NomeEstado=DISTRITO%20FEDERAL",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=32&NomeEstado=ESPIRITO%20SANTO",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=52&NomeEstado=GOIAS",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=21&NomeEstado=MARANHAO",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=51&NomeEstado=MATO%20GROSSO",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=50&NomeEstado=MATO%20GROSSO%20DO%20SUL",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=31&NomeEstado=MINAS%20GERAIS",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=15&NomeEstado=PARA",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=25&NomeEstado=PARAIBA",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=41&NomeEstado=PARANA",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=26&NomeEstado=PERNAMBUCO",
        "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=22&NomeEstado=PIAUI",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=33&NomeEstado=RIO%20DE%20JANEIRO",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=24&NomeEstado=RIO%20GRANDE%20DO%20NORTE",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=43&NomeEstado=RIO%20GRANDE%20DO%20SUL",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=11&NomeEstado=RONDONIA",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=14&NomeEstado=RORAIMA",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=42&NomeEstado=SANTA%20CATARINA",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=35&NomeEstado=SAO%20PAULO",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=28&NomeEstado=SERGIPE",
        # "http://cnes.datasus.gov.br/Lista_Tot_Es_Municipio.asp?Estado=17&NomeEstado=TOCANTINS"
    ]
    def phone_format(n):                                                                                                                                  
        return format(int(n[:-1]), ",").replace(",", "-") + n[-1]

    def parse(self, response):
        #parse de cada instituicao
        
        item = CnesItem()
        for name in response.xpath('//table[5]/tr[2]/td[1]/font[1]/text()').extract():
            item['nome'] = response.xpath("//table[5]/tr[2]/td[1]/font[1]/text()").extract()
            item['cnes'] = response.xpath("//table[5]/tr[2]/td[2]/font[1]/text()").extract()
            item['cnpj'] = response.xpath("//table[5]/tr[2]/td[3]/font[1]/text()").extract()
            item['nome_empresarial'] = response.xpath("//table[5]/tr[4]/td[1]/font[1]/text()").extract()
            item['logradouro'] = response.xpath("//table[5]/tr[6]/td[1]/font[1]/text()").extract()
            item['numero'] = response.xpath("//table[5]/tr[6]/td[2]/font[1]/text()").extract()
            item['complemento'] = response.xpath("//table[5]/tr[8]/td[1]/font[1]/text()").extract()
            item['bairro'] = response.xpath("//table[5]/tr[8]/td[2]/font[1]/text()").extract()
            item['cep'] = response.xpath("//table[5]/tr[8]/td[3]/font[1]/text()").extract()
            item['municipio'] = response.xpath("//table[5]/tr[8]/td[4]/a/font[1]/text()").extract()

            item['telefone'] = response.xpath("//table[5]/tr[6]/td[3]/font[1]/text()").extract()
            if(len(item['telefone']) > 5):
                telefone1 = item['telefone'].encode('utf-8')
                item['telefone'] = normalize('NFKD', cidade1.decode('utf-8')).encode('ASCII','ignore')

            path = urlparse(response.url).path
            
            if(path == "/Exibe_Ficha_Estabelecimento.asp"):
                query = parse_qs(urlparse(response.url).query)
                item['id_estado'] = query['VEstado'][0]
                item['id_cidade'] = query['VCodMunicipio'][0]
                
            item['uf'] = response.xpath("//table[5]/tr[8]/td[5]/font/text()").extract()
            
            if(len(item['uf']) == 0):
                continue;

            item['natureza_da_organizacao'] = response.xpath("//table[5]/tr[12]/td[1]/font/text()").extract()

            h = HTMLParser.HTMLParser()
            data_cadastrado = h.unescape(response.xpath("//table[4]/tr[1]/td[1]/font[1]/font[1]/text()").extract()[0])
            match_data_cadastrado = re.search(r'(\d+/\d+/\d+)', data_cadastrado.encode('utf-8'))
            if match_data_cadastrado is not None:
                item['data_cadastrado_cnes'] = match_data_cadastrado.group(0)

            data_atualizado = h.unescape(response.xpath("//table[4]/tr[1]/td[1]/font[1]/font[1]/text()").extract()[1])
            match_data_atualizado = re.search(r'(\d+/\d+/\d+)', data_atualizado.encode('utf-8'))
            if match_data_atualizado is not None:
                item['data_atualizado_cnes'] = match_data_atualizado.group(0)

            # item['string_atualizacao'] = m.group(0)
            item['tipo_estabelecimento'] = response.xpath("//table[5]/tr[10]/td[1]/font/text()").extract()
            item['subtipo_estabelecimento'] = response.xpath("//table[5]/tr[10]/td[2]/font/text()").extract()

            # item['cpf'] = response.xpath("//table[5]/tr[4]/td[2]/font[1]/text()").extract()
            # item['personalidade'] = response.xpath("//table[5]/tr[4]/td[2]/font[1]/text()").extract()
            
            yield CnesItem(item)

        #parse de cada estado e cada cidade
        for url in response.xpath('//table[5]/tr//a/@href').extract():
            base_url = 'http://cnes.datasus.gov.br/'
            url = base_url + url
            yield Request(url, callback=self.parse)