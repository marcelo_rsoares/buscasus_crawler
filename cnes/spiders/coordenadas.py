from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy import Request
from urlparse import urlparse, parse_qs
from cnes.items import CnesItemMap
import requests, json
import MySQLdb as mdb
import sys

class CnesSpiderMap(Spider):
    name =  "cnesmap"
    allowed_domains = ["datasus.gov.br"]

    con = mdb.connect('127.0.0.1', 'root', '', 'buscasus');
    cur = con.cursor()

    start_urls = [
    ]

    cur.execute("SELECT VUnidade FROM instituicao ORDER BY VUnidade ASC")
    data = cur.fetchall()

    if(len(data) > 0):
        for elem in data:
            start_urls.append("http://cnes.datasus.gov.br/geo.asp?VUnidade="+elem[0])

    def parse(self, response):
        #parse de cada instituicao
        item = CnesItemMap()
        for name in response.xpath('//input').extract():
            query = parse_qs(urlparse(response.url).query)
            item['VUnidade'] = query['VUnidade'][0]
            item['latitude'] = response.xpath("//input[1]/@value").extract()
            item['longitude'] = response.xpath("//input[2]/@value").extract()
            return CnesItemMap(item)