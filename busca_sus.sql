-- MySQL dump 10.13  Distrib 5.6.23, for osx10.10 (x86_64)
--
-- Host: localhost    Database: buscasus
-- ------------------------------------------------------
-- Server version	5.6.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cidade`
--

DROP TABLE IF EXISTS `cidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cidade` (
  `_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cidade`
--

LOCK TABLES `cidade` WRITE;
/*!40000 ALTER TABLE `cidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `cidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;

INSERT INTO `estado` (`_id`, `nome`, `uf`)
VALUES
  (11,'Rondônia','RO'),
  (12,'Acre','AC'),
  (13,'Amazonas','AM'),
  (14,'Roraima','RR'),
  (15,'Pará','PA'),
  (16,'Amapá','AP'),
  (17,'Tocantins','TO'),
  (21,'Maranhão','MA'),
  (22,'Piauí','PI'),
  (23,'Ceará','CE'),
  (24,'Rio Grande do Norte','RN'),
  (25,'Paraíba','PB'),
  (26,'Pernambuco','PE'),
  (27,'Alagoas','AL'),
  (28,'Sergipe','SE'),
  (29,'Bahia','BA'),
  (31,'Minas Gerais','MG'),
  (32,'Espírito Santo','ES'),
  (33,'Rio de Janeiro','RJ'),
  (35,'São Paulo','SP'),
  (41,'Paraná','PR'),
  (42,'Santa Catarina','SC'),
  (43,'Rio Grande do Sul','RS'),
  (50,'Matro Grosso do Sul','MS'),
  (51,'Matro Grosso','MT'),
  (52,'Goiás','GO'),
  (53,'Distrito Federal','DF');

/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instituicao`
--

DROP TABLE IF EXISTS `instituicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instituicao` (
  `_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(254) DEFAULT NULL,
  `cnes` varchar(30) DEFAULT NULL,
  `cnpj` varchar(22) DEFAULT NULL,
  `nome_empresarial` varchar(254) DEFAULT NULL,
  `logradouro` varchar(254) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `complemento` varchar(254) DEFAULT NULL,
  `bairro` varchar(254) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `municipio` varchar(254) DEFAULT NULL,
  `telefone` varchar(50) DEFAULT NULL,
  `uf` varchar(50) DEFAULT NULL,
  `natureza_da_organizacao` varchar(100) DEFAULT NULL,
  `tipo_estabelecimento` varchar(200) DEFAULT NULL,
  `subtipo_estabelecimento` varchar(200) DEFAULT NULL,
  `id_estado` varchar(20) DEFAULT NULL,
  `id_cidade` varchar(20) DEFAULT NULL,
  `VUnidade` varchar(20) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `id_tipo_estabelecimento` int(2) unsigned DEFAULT NULL,
  `urgencia` tinyint(1) unsigned DEFAULT '0',
  `sadt` tinyint(1) unsigned DEFAULT '0',
  `ambulatorial` tinyint(1) unsigned DEFAULT '0',
  `internacao` tinyint(1) unsigned DEFAULT '0',
  `data_atualizacao` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `data_cadastrado_cnes` date DEFAULT NULL,
  `data_atualizado_cnes` date DEFAULT NULL,
  `qtde_leitos` int(5) DEFAULT NULL,
  `uti` tinyint(1) DEFAULT NULL,
  `publica` tinyint(1) DEFAULT NULL,
  `demanda` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instituicao`
--

LOCK TABLES `instituicao` WRITE;
/*!40000 ALTER TABLE `instituicao` DISABLE KEYS */;
/*!40000 ALTER TABLE `instituicao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_estabelecimento`
--

DROP TABLE IF EXISTS `tipo_estabelecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_estabelecimento` (
  `_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_estabelecimento`
--

LOCK TABLES `tipo_estabelecimento` WRITE;
/*!40000 ALTER TABLE `tipo_estabelecimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_estabelecimento` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-03 12:15:44
